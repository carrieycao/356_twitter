
"use strict";

var mePage = {
    username: "",
    speakIdToDelete: 0,
    postNewFailed: false
};



$(document).ready(function(){
    // TODO: change to get only following and current user (sort by timestamp)
    // getSpeakFeed();
    getAllSpeaks();
    initMeEventHandlers();
    mePage.username = $("#username").html();
});

function initMeEventHandlers(){
    initEventHandlers();
    $("#postSpeakBtn").click(postSpeak);
    $("#confirmDeleteBtn").click(deleteSpeak);

    $("#searchForm").submit(function(e){
        e.preventDefault();
        submitSearchForm($("#searchForm"));
    });

    $("form#postSpeakForm").submit(function(){
        var formData = new FormData($(this)[0]);
        if(document.getElementById('content').files.length == 0 ){
            return false;
        }
        this.reset();
        $.ajax({
            url: "/addmedia",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {

                if(data["status"] == "OK"){
                    $("#mediaID").val(data["id"]);
                } else {
                    $("#errorMsgModalTitle").html("Post new Speak failed");
                    $("#errorMsgModalBody").html(data["error"]);
                    $("#errorMsgModal").modal();
                    mePage.postNewFailed = true;
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });

}

function submitSearchForm($form){
     var formData =$form.serializeArray() ;
     var data = {};
     for(var i = 0; i < formData.length; i++){
         var item = formData[i];
         data[item["name"]] = item["value"];
     }
    data["following"] = false;

     $.ajax({
         type: "POST",
         url: "/search",
         data: JSON.stringify(data),
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(msg) {
            if(msg["status"] == "error"){
                $("#errorMsgModalTitle").html("Search failed");
                $("#errorMsgModalBody").html(msg["error"]);
                $("#errorMsgModal").modal();

            } else {
                $("#speakFeedList").html('<li class="collection-item active"><h3 id="ownSpeakIndicator">Search result</h3></li>');
                updateSpeaks(msg["items"]);
            }

         },
         error: function(data) {
             console.log(data);
             alert("error in connecting to server");
         },
         statusCode: {
             404: function() {
               alert('page not found');
             },
             500: function() {
                alert("Server error\nIs the program being run elsewhere at the same time?");
            }

         }
     });
 }

function deleteSpeakModal(id){
    mePage.speakIdToDelete = id;
    $("#deleteSpeakModal").modal();

}

function updateSpeaksCount(offset){
    var newSpeaksCount = parseInt($("#speaksCount").html()) + offset;
    $("#speaksCount").html(newSpeaksCount);
}

function deleteSpeak(){
    $.ajax({
        type: "DELETE",
        url: "/item/" + mePage.speakIdToDelete,
        success: function(msg) {
            console.log(msg);
            if(msg["status"] == "OK"){
                $("#speak_id_" + mePage.speakIdToDelete).remove();
                updateSpeaksCount(-1);
            } else {
                $("#errorMsgModalTitle").html("Failed to delete");
                $("#errorMsgModalBody").html(msg["error"]);
                $("#errorMsgModal").modal();
            }

        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}


//post to add a new Speak
function postSpeak(){
    mePage.postNewFailed = false;
    var input = $("#newSpeakContent").val();
    $("#newSpeakContent").val("");
    $("form#postSpeakForm").submit();

    if(mePage.postNewFailed){
        return;
    }
    var data = {"content": input};
    var mediaID = $("#mediaID").val();
    if(mediaID !== null && mediaID.length != 0){
        data["media"] = [mediaID];
        $("#mediaID").val("");
        console.log(mediaID);
    }

	$.ajax({
		type: "POST",
		url: "/additem",
		data: JSON.stringify(data),
		contentType: "application/json; charset=utf-8",
        dataType: "json",
		success: function(msg) {
		    console.log(msg);
		    if(msg["status"] == "error"){
		        $("#errorMsgModalTitle").html("Failed to post new Speak");
		        $("#errorMsgModalBody").html(msg["error"]);
		        $("#errorMsgModal").modal();
		    } else {
                updateSpeaksCount(1);
                getOwnSpeaks();
		    }
		},
		statusCode: {
			404: function() {
			  alert('page not found');
			},
			500: function() {
			   alert("Server error\nIs the program being run elsewhere at the same time?");
		   }
		  
		}
	});
}




function getItem(){
    var id = $("#itemID").val();
    console.log(id);
    $.ajax({
        type: "GET",
        url: "/item/" + id,
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            $("#sessionTable tbody").html("");
            var item = msg.item;
            var id = item.id;
            var username = item.username;
            var content = item.content;
            var timestamp = item.timestamp;
            var date = timeConverter(timestamp);
            $("#sessionTable tbody").append('<tr> <th scope="row">' + id + '</th> <td>' +
                    username + '</td> <td>' + content + '</td> <td>' + date + '</td> </tr>');

            $("#sessionListModal").modal();


        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}




function getAllSpeaks(callback){
    $.ajax({
        type: "POST",
        url: "/search",
        data: JSON.stringify({"rank": "time"}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            if(msg["items"].length == 0){
                $("#speakFeedList").html(
                    '<li class="collection-item"> You have nothing here.</li>'
                );
            } else {
                $("#speakFeedList").html("");
                updateSpeaks(msg["items"], callback);

            }
            $("#speakFeedList").prepend('<li class="collection-item active"><h3 id="ownSpeakIndicator">Your feed</h3></li>');

        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}
