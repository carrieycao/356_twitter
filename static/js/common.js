
"use strict";

var commonPage ={
    username: "",
    likedStarClass: "fa-star",
    notLikedStarClass: "fa-star-o",
    postReplyFailed: false,
}

$(document).ready(function(){
    commonPage.username = $("#username").html();
    $("#logOutButton").click(logOut);
    $("#myProfileButton").click(goToMyPage);
    $("#postReplyBtn").click(postReply);

});

function initEventHandlers(){
    $("form#replyForm").submit(function(){
        var formData = new FormData($(this)[0]);
        if(document.getElementById('mediaContent').files.length == 0 ){
            return false;
        }
        this.reset();
        $.ajax({
            url: "/addmedia",
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {

                if(data["status"] == "OK"){
                    $("#replyMediaID").val(data["id"]);
                } else {
                    $("#errorMsgModalTitle").html("Post new Speak failed");
                    $("#errorMsgModalBody").html(data["error"]);
                    $("#errorMsgModal").modal();
                    commonPage.postReplyFailed = true;
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });

}


//post a reply
function postReply(){
    commonPage.postReplyFailed = false;
    var input = $("#replyContent").val();
    $("#replyContent").val("");
    $("form#replyForm").submit();

    if(commonPage.postReplyFailed){
        return;
    }
    var parent_id = $("#orig_tweet_list").find("li").attr("id");
    var data = {"content": input, "parent": parent_id};
    var mediaID = $("#replyMediaID").val();
    if(mediaID !== null && mediaID.length != 0){
        data["media"] = [mediaID];
        $("#replyMediaID").val("");
    }

	$.ajax({
		type: "POST",
		url: "/additem",
		data: JSON.stringify(data),
		contentType: "application/json; charset=utf-8",
        dataType: "json",
		success: function(msg) {
		    console.log(msg);
		    if(msg["status"] == "error"){
		        $("#errorMsgModalTitle").html("Failed to post new Speak");
		        $("#errorMsgModalBody").html(msg["error"]);
		        $("#errorMsgModal").modal();
		    } else {
                updateSpeaksCount(1);
                getOwnSpeaks();
		    }
		},
		statusCode: {
			404: function() {
			  alert('page not found');
			},
			500: function() {
			   alert("Server error\nIs the program being run elsewhere at the same time?");
		   }

		}
	});
}

function getOwnSpeaks(){

    var username = commonPage.username;
    $.ajax({
        type: "POST",
        url: "/ui/search",
        data: JSON.stringify({
            "following": false,
            "username": username,
            "rank": "time"
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {

            if(msg["items"].length == 0){
                $("#speakFeedList").html(
                    '<li class="collection-item"> This user has not spoke about anything.</li>'
                );
            } else {
                $("#speakFeedList").html("");
                updateSpeaks(msg["items"]);
            }
            $("#speakFeedList").prepend('<li class="collection-item active"><h3 id="ownSpeakIndicator">'
                            + username + '\'s speaks</h3></li>');


        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}

function getFollowers(){
    var username = commonPage.username;
    $.ajax({
        type: "GET",
        url: "/user/" + username + "/followers",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            var userArr = msg["users"];
            if( userArr === undefined || userArr.length == 0){
                $("#speakFeedList").html('<li class="collection-item active"><h3 id="ownSpeakIndicator">'
                                            + username + ' does not have any followers</h3></li>');
            } else {
                $("#speakFeedList").html('<li class="collection-item active"><h3 id="ownSpeakIndicator">'
                            + username + '\'s followers</h3></li>');
                        displayUsers(msg["users"]);
            }
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });

}

function getFollowing(){
    var username = commonPage.username;
    $.ajax({
        type: "GET",
        url: "/user/" + username + "/following",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            var userArr = msg["users"];
            if( userArr === undefined || userArr.length == 0){
                $("#speakFeedList").html('<li class="collection-item active"><h3 id="ownSpeakIndicator">'
                                            + username + ' is not following anyone</h3></li>');
            } else {
                $("#speakFeedList").html('<li class="collection-item active"><h3 id="ownSpeakIndicator">'
                            + username + '\'s following</h3></li>');
                        displayUsers(msg["users"]);
            }


        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });

}
//precondition: useArr is not undefined
function displayUsers(userArr){

    var length = userArr.length;
    for(var i = 0; i < length; i++) {
        var username = userArr[i];
        $("#speakFeedList").append(
            userListHtmlFormat(username)
        );

    }
}

function updateSpeaks(speaksArr, callback){
    var length = speaksArr.length;
    for(var i = 0; i < length; i++) {
        var speakItem = speaksArr[i];
        $("#speakFeedList").append(
            speakHtmlFormat(speakItem)
        );

    }



    if (callback !== undefined) {
        callback();
    }

}

function userListHtmlFormat(username){

    return `
       <li class="collection-item">
           <a class="speak-user" onclick="getUserProfile('`+ username +`')">
               <span class="speak-username">` + username + `</span>
           </a>
       </li>
    `;


}

function likeSpeak(itemID, e){
    var toLike = true;
    if( $(e).hasClass(commonPage.likedStarClass)){
        toLike = false;
    }
    $.ajax({
        type: "POST",
        url: "/item/"+itemID+"/like",
        data: JSON.stringify({
            "like": toLike
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            if(msg["status"] == "error"){
                $("#errorMsgModalTitle").html("Failed to like this post");
                $("#errorMsgModalBody").html(msg["error"]);
                $("#errorMsgModal").modal();
            } else {
                //update icon
                if(toLike){
                    // liked this post
                    $(e).addClass(commonPage.likedStarClass);
                    $(e).removeClass(commonPage.notLikedStarClass);
                    $(e).next(".like-count").html( parseInt($(e).next(".like-count").html()) + 1);
                } else {
                    // unliked this post
                    $(e).addClass(commonPage.notLikedStarClass);
                    $(e).removeClass(commonPage.likedStarClass);
                    $(e).next(".like-count").html( parseInt($(e).next(".like-count").html()) - 1);
                }
            }


        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });

}


function respeak(e, id){
    var content = $("#speak_id_"+id).find(".speak-content").html();
    content = "RT: " + content.replace(/<br>/g, "\n")
    $.ajax({
        type: "POST",
        url: "/respeak",
        data: JSON.stringify({ "content": content, "orig_item_id": id}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(msg) {
            console.log(msg);
            if(msg["status"] == "error"){
                $("#errorMsgModalTitle").html("Failed to post new Speak");
                $("#errorMsgModalBody").html(msg["error"]);
                $("#errorMsgModal").modal();
            } else {
                $(e).next(".respeak-count").html( parseInt($(e).next(".respeak-count").html()) + 1);
            }
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });

}

// display reply modal
function showReplyModal(orig_id){
    var $orig_speak = $("#speak_id_" + orig_id).clone().attr("id",orig_id);
    $("#orig_tweet_list").html($orig_speak);
    $("#replyModal").modal();
}

function speakHtmlFormat(speakItem, viewConvMode){
    if(viewConvMode == null){
        viewConvMode = false;
    }
    var id = speakItem["id"];
    var username = speakItem["username"];
    var content = speakItem["content"];
    var date = getSpeakTImeStr( Math.floor(Date.now() / 1000), speakItem["timestamp"]);
    var loggedInUser = commonPage.username;
    var liked = speakItem["liked"];
    var num_likes = speakItem["num_likes"];
    var num_respeaks = speakItem["num_retweets"];
    var media_ids = speakItem["media"];
    var image = "";
    var parent_id = speakItem["parent"];
    var orig_tweet_link = "";

    if(media_ids != null && media_ids.length != 0){
        image = '<img src="http://'+window.location.host+'/media/'+media_ids[0]+'" class="speak-img" >';
    }

    if(!viewConvMode && parent_id != null && parent_id.length != 0 && parent_id.toLowerCase() != "none"){
        orig_tweet_link = '<a class="reply-link" onclick="getConv(\''+id+'\')">view conv</a>'
    }

    var starClass = commonPage.notLikedStarClass;
    if(liked){
        starClass = commonPage.likedStarClass;
    }


    var html = `
       <li class="collection-item avatar" id="speak_id_`+id+`">
           <a class="speak-user" onclick="getUserProfile('`+ username +`')">
               <img src="http://0.gravatar.com/avatar/60efa32c26a19f3ed2e42798afb705ba?s=100&amp;d=mm&amp;r=g"
                    alt="" class="circle">
               <span class="speak-username">` + username + `</span>
           </a>
           <span class="speak-post-date">` + date +`</span>`
           + orig_tweet_link + image +
           `
           <p class="speak-content">`  + content.replace(/\n/g, "<br>") + `
           </p>
           <div>
                <span class="speak-action-icon-wrapper">
                    <i class="fa `+ starClass +` like-btn" aria-hidden="true" onclick="likeSpeak('`+id+`', this)"></i>
                    <span class="like-count">`+num_likes+`</span>
                </span>
                <span class="speak-action-icon-wrapper">
                    <i class="fa fa-retweet respeak-btn" aria-hidden="true" data-dismiss="modal" onclick="respeak(this, '`+id+`')"></i>
                    <span class="respeak-count">`+num_respeaks+`</span>
                </span>
                <span class="speak-action-icon-wrapper">
                    <i class="fa fa-reply reply-btn" aria-hidden="true" data-dismiss="modal" onclick="showReplyModal('`+id+`')"></i>
                </span>
                `
    if(loggedInUser !== undefined && loggedInUser == username){

        html +=   `<span class="speak-action-icon-wrapper">
                        <i class="fa fa-times delete-btn" aria-hidden="true" data-dismiss="modal" onclick="deleteSpeakModal('`+ id +`')"></i>
                    </span>`;
    }

    html +=   `
           </div>
       </li>
    `;

    return html;
}


function getConv(itemID){
    $("#orig_speak_conv").html("");
    $.ajax({
        type: "GET",
        url: "/getconv/"+itemID,
        success: function(data) {
            //console.log(data);
            var items = data["items"];
            for(var i = 0; i < items.length; i++){
                var html = speakHtmlFormat(items[i], true);
                $("#orig_speak_conv").prepend(html);
            }
            $("#originalSpeakModal").modal();
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}

function getUserProfile(username){
    window.location = "/ui/user/" + username;
}

function notImplemented(){
    alert("to be implemented");
}

function goToMyPage(){
    window.location = "/me";
}

function logOut(){
    $.ajax({
        type: "POST",
        url: "/logout",
        error: function(data) {
            console.log(data);
            alert("error in connecting to server");
        },
        success: function(msg) {
            window.location = msg.redirect;
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}


function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time =  month + ' ' + date + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}


function getSpeakTImeStr(current, previous) {

    var msPerMinute = 60 ;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
         return Math.round(elapsed) + ' s';
    }

    else if (elapsed < msPerHour) {
         return Math.round(elapsed/msPerMinute) + ' m';
    }

    else if (elapsed < msPerDay ) {
         return Math.round(elapsed/msPerHour ) + ' h';
    }

    else {
        var a = new Date(previous * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var month = months[a.getMonth()];
        var date = a.getDate();
        var time =  month + ' ' + date;
        return time;

    }
}