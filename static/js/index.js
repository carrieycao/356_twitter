"use strict";

var signUpMode = false;
var passedRecaptcha = false;

$( document ).ready(function() {
    initEventHandlers();

       

});

function initEventHandlers(){
    $("#switchButton").click(switchMode);
    $("#homeIcon").click(refreshPage);

    $("#accountForm").submit(function(e){
        e.preventDefault();
        submitForm($("#accountForm"));
    });
}

// switching between sign up mode and log in mode
function switchMode(){
    $("#errorMessages").slideUp();
    signUpMode = ! signUpMode;
    if(signUpMode){
        // show UI for sign up

        $(".sign-up-only").slideDown(function(){
            $(".sign-up-only #inputEmail").attr('required','required');
        });
        if(!passedRecaptcha){
            $("#submitBtn").slideUp();
        }

        changeText($("#submitButtonText"), "Sign Up");

        changeText($("#switchButtonActionText1"), "Have an account?");
        changeText($("#switchButtonActionText2"), "Sign In");

    } else {
        $("#submitBtn").slideDown();
        // log in
        $(".sign-up-only input").removeAttr('required');
        $(".sign-up-only input").val('');
        $(".sign-up-only").slideUp();
        changeText($("#submitButtonText"), "Sign in");

        changeText($("#switchButtonActionText1"), "Not a member?");
        changeText($("#switchButtonActionText2"), "Sign Up");
    }

}

// either post to login or adduser depending on signUpMode
function submitForm($form){
    $("#errorMessages").slideUp();
    console.log("in submitForm");
    var formData =$form.serializeArray() ;
    var url = "/login";
    if(signUpMode){
        url = "/adduser";
    }

    var data = {};
    for(var i = 0; i < formData.length; i++){
        var item = formData[i];
        data[item["name"]] = item["value"];
    }


    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(msg) {
            if(msg.redirect != null){ // for login
                window.location = msg.redirect;
            }
            else if(msg.status == "OK"){
                $("#accountForm").html("<h3>" + msg.msg + " </h3>");
            } else {
                $("#errorMessages").html(msg.error).slideDown();
            }
        },
        error: function(data) {
            console.log(data);
            alert("error in connecting to server");
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}

function refreshPage(){
    window.location.reload();
}

function passedCaptcha(){
    passedRecaptcha = true;
    $("#submitBtn").slideDown();
}


// animates text change
function changeText($elm, newText){
    $elm.fadeOut(function(){
        $elm.html(newText);
        $elm.fadeIn(800);
    });
}