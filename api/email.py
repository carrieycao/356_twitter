####################
# Sends email
####################

# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText
import smtplib

gmail_user = 'md.eliza.doctor@gmail.com'
gmail_password = 'iameliza'


def send(to_adr, subject, body):
    to = [to_adr]

    header = 'To:' + ", ".join(to) + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:' + subject + ' \n'
    email_text = header + '\n' + body

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(gmail_user, to, email_text)
        server.close()

        print('Email sent!')
    except:
        print('Something went wrong...')

