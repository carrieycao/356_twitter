
run top

check logs:
	nginx
	uWSGI
	db_other
	shards
	configsvr
	num of mongos connections

check value of i

scp -i ccao.pem ubuntu@130.245.168.201:~/data/logs/uwsgi.log fe1_uwsgi.log
scp -i ccao.pem ubuntu@130.245.168.203:~/data/logs/uwsgi.log fe2_uwsgi.log
scp -i ccao.pem ubuntu@130.245.168.200:~/data/logs/uwsgi.log fe3_uwsgi.log

scp -i ccao.pem ubuntu@130.245.168.204:/var/log/nginx/access.log nginx.log


C:/Python27/Scripts/uwsgi-sloth analyze -f ~/Downloads/fe1_uwsgi.log --output=fe1.html --min-msecs=400
C:/Python27/Scripts/uwsgi-sloth analyze -f ~/Downloads/fe2_uwsgi.log --output=fe2.html --min-msecs=400
C:/Python27/Scripts/uwsgi-sloth analyze -f ~/Downloads/fe3_uwsgi.log --output=fe3.html --min-msecs=400
C:/Python27/Scripts/uwsgi-sloth analyze -f ~/Downloads/fe4_uwsgi.log --output=fe4.html --min-msecs=1000
	
=== resubmit ===

# clear db (tweet and other)

mongo 192.168.1.175:27017/twitter
db.follow_table.remove({})
db.like_table.remove({})
db.media_table.remove({})
db.tweet_table.remove({})
db.user_table.remove({})


mongo 192.168.1.127:27017/twitter
db.follow_table.remove({})
db.like_table.remove({})
db.media_table.remove({})
db.tweet_table.remove({})
db.user_table.remove({})


# rerun uwsgi


sudo mv ~/data/logs/uwsgi.log ~/data/logs/uwsgi.log.prev
killall -KILL uwsgi
cd ~/356_twitter
git pull
source spikkerenv/bin/activate
uwsgi --socket 0.0.0.0:5000 --protocol=http -w spikker:application --processes 100 --threads 10  --enable-threads --master --wsgi-disable-file-wrapper --daemonize ~/data/logs/uwsgi.log



# remove old nginx logs

sudo mv /var/log/nginx/access.log /var/log/nginx/access.log.prev
sudo mv /var/log/nginx/error.log /var/log/nginx/error.log.prev

sudo service nginx restart
sudo service nginx status



# rotate db logs 
mongo localhost:37017
use admin
db.runCommand( { logRotate : 1 } )
db.setProfilingLevel(2, 100)


