
"use strict";

var userPage = {
    toFollow: true
};

$(document).ready(function(){
    getOwnSpeaks();
	initUserEventHandlers();
	userPage.toFollow = $("#followBtn").text().toUpperCase() == "FOLLOW";
});

function initUserEventHandlers(){
    initEventHandlers();
    $("#followBtn").click(follow);
}


function follow(){
    var username = $("#username").html();
    console.log("toFollow: " + userPage.toFollow);
    $.ajax({
        type: "POST",
        url: "/follow",
        data: JSON.stringify({
            "follow": userPage.toFollow,
            "username": username
            }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            if(msg["status"] == "OK"){
                userPage.toFollow = !userPage.toFollow;
                var followCount = $("#followersCount").html();
                if(userPage.toFollow){
                    $("#followBtn").html("Follow");
                    followCount--;
                } else {
                    $("#followBtn").html("Unfollow");
                    followCount++;
                }
                $("#followersCount").html(followCount);

            } else {
                $("#errorMsgModalTitle").html("Failed to follow or unfollow");
                $("#errorMsgModalBody").html(msg["error"]);
                $("#errorMsgModal").modal();
            }
        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });

}

function getItem(){
    var id = $("#itemID").val();
    console.log(id);
    $.ajax({
        type: "GET",
        url: "/item/" + id,
        error: function(data) {
            alert("error in connecting to server");
        },
        success: function(msg) {
            $("#sessionTable tbody").html("");
            var item = msg.item;
            var id = item.id;
            var username = item.username;
            var content = item.content;
            var timestamp = item.timestamp;
            var date = timeConverter(timestamp);
            $("#sessionTable tbody").append('<tr> <th scope="row">' + id + '</th> <td>' +
                    username + '</td> <td>' + content + '</td> <td>' + date + '</td> </tr>');

            $("#sessionListModal").modal();


        },
        statusCode: {
            404: function() {
              alert('page not found');
            },
            500: function() {
               alert("Server error\nIs the program being run elsewhere at the same time?");
           }

        }
    });
}

function appendSpeaks(speaksArr, callback){
    console.log(speaksArr);

    var currentTimeStamp = Math.floor(Date.now() / 1000);

    var length = speaksArr.length;
    for(var i = 0; i < length; i++) {
        var speakItem = speaksArr[i];
        var date = getSpeakTImeStr(currentTimeStamp, speakItem["timestamp"]);
        $("#speakFeedList").append(
            speakHtmlFormat(speakItem["id"], speakItem["username"], speakItem["content"], date)
        );
    }
    callback();
}





