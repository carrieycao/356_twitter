#!/usr/bin/env python
from __future__ import print_function

from datetime import datetime
from flask import Flask, request, session, \
    redirect, render_template, jsonify, escape, send_file, make_response
import api
import api.email
import sys
import io

application = Flask(__name__, static_url_path='')
application.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

port_number = 5000
if len(sys.argv) > 1:
    port_number = int(sys.argv[1])

"""

uwsgi --socket 0.0.0.0:5000 --protocol=http -w spikker:application --processes 100


When the user logs in, store username in session
When the user logs out, remove the username

When checking if the user is logged in, check if username exists in session

"""


@application.before_request
def before_request():
    session.permanent = True


#####################################################################
# serves the index page
@application.route(api.INDEX_PAGE_URL)
def index_page():
    # redirect to main page if logged in
    username = is_logged_in()
    return render_template(api.INDEX_PAGE_FILE_NAME) if username is None \
        else redirect(api.MAIN_PAGE_URL)


#####################################################################
# serves the main page
@application.route(api.MAIN_PAGE_URL)
def main_page():
    # check if username is in session and is not empty
    username = is_logged_in()
    # redirect back to index page if not logged in
    if username is None:
        return redirect(api.INDEX_PAGE_URL)

    resp = api.get_user(username)
    speaks_count = api.get_number_speaks(username)
    followers_count = resp["user"]["followers"]
    following_count = resp["user"]["following"]

    return render_template(api.MAIN_PAGE_FILE_NAME,
                           username=username,
                           speaks_count=speaks_count,
                           followers_count=followers_count,
                           following_count=following_count
                           )


#####################################################################
# check if the current user is logged in
# return the username if logged in.
# return None otherwise
def is_logged_in():
    if 'username' in session:
        return escape(session['username'])  # todo: check if escape is necessary
    else:
        return None


#####################################################################
# Register new user account
# Username and email must be unique
# Should send email with verification key
#
# Request Params (POST parameters are JSON)
# 	- username
# 	- email
# 	- password
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	error: error message (if error)
# 	msg:  message (if ok)
@application.route("/adduser", methods=['POST'])
def add_user():
    json_obj = request.json
    username = json_obj["username"]
    email = json_obj["email"]
    password = json_obj["password"]

    resp = api.add_user(username, email, password)  # resp_json returns a dictionary
    # use email_api.py to send email
    # todo: use a new thread to send email
    if resp["status"] == "OK":
        content = request.url_root + "verify?email=" + email + "&key=" + resp["key"]
        #api.email.send(email, "Verify your account", content)

    return jsonify(resp)


#####################################################################
# Login to account
# Sets session cookie
#
# Request Params (POST parameters are JSON)
# 	- username
# 	- password
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	error: error message (if error)
#   redirect: link to the main page (if ok)
@application.route("/login", methods=['POST'])
def login():
    json_obj = request.json
    username = json_obj["username"]
    password = json_obj["password"]

    resp = api.login(username, password)

    if resp["status"] == "OK":  # set username
        session['username'] = username
        print("logged in: user " + username)

    return jsonify(resp)


#####################################################################
# Logout of account
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	error: error message (if error)
@application.route("/logout", methods=['POST'])
def logout():
    username = is_logged_in()
    session.pop('username', None)  # remove username
    if username is not None:  # check if logged in
        resp = {
            "status": "OK",
            "redirect": api.INDEX_PAGE_URL
        }
        print("logged out: user " + username)
    else:  # not logged in
        resp = {
            "status": "error",
            "error": "Cannot log out if the user is not logged in"
        }
    return jsonify(resp)


#####################################################################
# Verifies account
# Account cannot be used until account is verified
#
# Request Params (POST parameters are JSON)
# 	- email
# 	- key
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	error: error message (if error)
#   msg: message (if success)
@application.route("/verify", methods=['POST', 'GET'])
def verify():
    if request.method == 'POST':
        json_obj = request.json
        email = json_obj["email"]
        key = json_obj["key"]

        resp = api.verify(email, key)  # resp_json returns a dictionary
        return jsonify(resp)
    else:
        email = request.args.get("email")
        key = request.args.get("key")

        resp = api.verify(email, key)  # resp_json returns a dictionary
        message = resp["msg"] if resp["status"] == "OK" else resp["error"]
        return render_template(api.MESSAGE_TEMPLATE, message=message)


#####################################################################
# Post a new tweet
# Only allowed if logged in
#
# Request Params (POST parameters are JSON)
# 	- content: message body of tweet
#
# Returns (JSON):
# Returns (JSON):
# 	status: "OK" or "error"
# 	id: unique tweet ID (if OK)
# 	error: error message (if error)
@application.route("/additem", methods=['POST'])
def add_item():
    insert_date = datetime.now()
    # check if the user is logged in
    username = is_logged_in()
    if username is None:
        resp = {
            "status": "error",
            "error": "add_item: user is not logged in"
        }
        return jsonify(resp)

    json_obj = request.json
    content = json_obj["content"]
    parent = json_obj["parent"] if "parent" in json_obj else None
    media = json_obj["media"] if "media" in json_obj else None

    resp = api.add_item(content, parent, media, username, insert_date)  # resp_json returns a dictionary

    return jsonify(resp)


#####################################################################
# Post a new tweet
# Only allowed if logged in
#
# Request Params (POST parameters are JSON)
# 	- content: message body of tweet
#
# Returns (JSON):
# Returns (JSON):
# 	status: "OK" or "error"
# 	id: unique tweet ID (if OK)
# 	error: error message (if error)
@application.route("/respeak", methods=['POST'])
def respeak():
    respeak_date = datetime.now()
    # check if the user is logged in
    username = is_logged_in()
    if username is None:
        resp = {
            "status": "error",
            "error": "add_item: user is not logged in"
        }
        return jsonify(resp)

    json_obj = request.json
    content = json_obj["content"]
    orig_item_id = json_obj["orig_item_id"]

    resp = api.respeak(content, orig_item_id, username, respeak_date)  # resp_json returns a dictionary

    return jsonify(resp)


#####################################################################
# Delete an item
# Only allowed if logged in
#
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	error: error message (if error)
@application.route('/item/<item_id>', methods=['DELETE'])
def delete_item(item_id):
    # check that the user is logged in
    username = is_logged_in()
    if username is None:
        resp = {
            "status": "error",
            "error": "delete_item: no logged in user"
        }
    else:
        # the user is logged in but not sure if this tweet belongs to user
        resp = api.delete_item(item_id, username)
    return jsonify(resp)


#####################################################################
# Get contents of a single tweet given an ID
#
# Returns
# 	status: "OK" or "error"
# 	item: {
# 		id: tweet id
# 		username: username who sent tweet
# 		content: message body of tweet
# 		timestamp: timestamp, represented as Unix time in seconds
# 	}
# 	error: error message (if error)
@application.route('/item/<item_id>', methods=['GET'])
def get_item(item_id):
    resp = api.get_item(item_id)
    return jsonify(resp)


#####################################################################
# Gets user profile information
#
# Returns
# 	status: "OK" or "error"
# 	user: {
# 		email
#       followers: follower count
#       following: following count
# 	}
# 	error: error message (if error)
@application.route('/user/<username>', methods=['GET'])
def get_user(username):
    resp = api.get_user(username)
    return jsonify(resp)


@application.route('/ui/user/<username>', methods=['GET'])
def get_user_ui(username):
    resp = api.get_user(username)
    speaks_count = api.get_number_speaks(username)
    followers_count = resp["user"]["followers"]
    following_count = resp["user"]["following"]
    follow_button_text = "Follow"  # TODO: CHANGE TO "Unfollow" if already followed
    current_user = is_logged_in()
    if current_user is not None:
        follow_button_text = api.get_follow_btn_text(current_user, username)

    return render_template(api.USER_PAGE_FILE_NAME,
                           username=username,
                           speaks_count=speaks_count,
                           followers_count=followers_count,
                           following_count=following_count,
                           follow_button_text=follow_button_text
                           )


#####################################################################
# Follow or unfollow a user
# Only allowed if logged in
#
# Request Params (POST parameters are JSON)
# 	- username: username to follow
#   - follow: Boolean. Default: true
#
# Returns (JSON):
# 	status: "OK" or "error"
#   error: error message if any
@application.route("/follow", methods=['POST'])
def follow():
    # todo: check username exists, is not current user, and is not already followed/unfollowed

    # check if user is logged in
    current_user = is_logged_in()
    if current_user is None:
        return jsonify({
            "status": "error",
            "error": "follow: no logged in user"
        })

    json_obj = request.json
    username = json_obj["username"]
    to_follow = True

    if "follow" in json_obj:
        to_follow = json_obj["follow"]

    resp = api.follow(current_user, username, to_follow)
    return jsonify(resp)


#####################################################################
# Gets list of users following "username"
#
# Params:
#   limit:
#           number of usernames to return
#           Integer, optional
#           Default: 50
#           Max: 200

# Returns
# 	status: "OK" or "error"
# 	users:  list of usernames (strings)
# 	error: error message (if error)
@application.route('/user/<username>/followers', methods=['GET'])
def get_user_followers(username):
    limit = 50
    max_limit = 200

    if request.args.get('limit') is not None:
        limit = request.args.get('limit')
        # make sure that the maximum is 100
        if limit > max_limit:
            limit = max_limit

    resp = api.get_user_followers(username, limit)
    return jsonify(resp)


#####################################################################
# Gets list of users "username" is following
#
# Params:
#   limit:
#           number of usernames to return
#           Integer, optional
#           Default: 50
#           Max: 200

# Returns
# 	status: "OK" or "error"
# 	users:  list of usernames (strings)
# 	error: error message (if error)
@application.route('/user/<username>/following', methods=['GET'])
def get_user_following(username):
    limit = 50
    max_limit = 200

    # check if limit exists
    if request.args.get('limit') is not None:
        limit = request.args.get('limit')
        # make sure that the maximum is 100
        if limit > max_limit:
            limit = max_limit

    resp = api.get_who_user_is_following(username, limit)
    return jsonify(resp)


#####################################################################
# Gets a list of all tweets
#
# Request Params (POST parameters are JSON)
# 	timestamp: search tweets from this time and earlier
# 		Represented as Unix time in seconds
# 		Integer, optional
# 		Default: Current time
# 	limit: number of tweets to return
# 		Integer, optional
# 		Default: 25
# 		Max: 100
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	items: Array of tweet objects (see /item/:id)
# 	error: error message (if error)
@application.route("/search", methods=['POST'])
def search():
    # check of the user is logged in
    current_user = is_logged_in()
    if current_user is None:
        return jsonify({
            "status": "error",
            "error": "search: no logged in user",
            "items": []
        })

    # The user is logged in, from this line forward.
    json_obj = request.json
    # default values
    timestamp = datetime.now()
    limit = 25
    q = None
    username = None
    following = True
    rank = api.RANK_BY_INTEREST
    replies = True
    parent = None

    if json_obj is not None:
        # check if these parameters exist

        # check if limit exists
        if "limit" in json_obj:
            limit = json_obj["limit"]
            # make sure that the maximum is 100
            if limit > 100:
                limit = 100

        if "q" in json_obj:
            q = json_obj["q"]

        if "username" in json_obj:
            username = json_obj["username"]

        if "following" in json_obj:
            following = json_obj["following"]

        if "rank" in json_obj and json_obj["rank"] == api.RANK_BY_TIME:
            rank = json_obj["rank"]

        # only if json_obj["replies"] is False
        if "replies" in json_obj and not json_obj["replies"]:
            replies = json_obj["replies"]

        if "parent" in json_obj:
            parent = json_obj["parent"]

    resp = api.search(current_user, timestamp, limit, q, username, following, rank, parent, replies)
    return jsonify(resp)


#####################################################################
# Gets a list of all tweets (with like status for UI)
#
# Request Params (POST parameters are JSON)
# 	timestamp: search tweets from this time and earlier
# 		Represented as Unix time in seconds
# 		Integer, optional
# 		Default: Current time
# 	limit: number of tweets to return
# 		Integer, optional
# 		Default: 25
# 		Max: 100
#
# Returns (JSON):
# 	status: "OK" or "error"
# 	items: Array of tweet objects (see /item/:id), with liked: boolean
# 	error: error message (if error)
@application.route("/ui/search", methods=['POST'])
def ui_search():
    # check of the user is logged in
    current_user = is_logged_in()
    if current_user is None:
        return jsonify({
            "status": "error",
            "error": "search: no logged in user",
            "items": []
        })

    # The user is logged in, from this line forward.
    json_obj = request.json
    # default values
    timestamp = datetime.now()
    limit = 25
    q = None
    username = None
    following = True
    rank = api.RANK_BY_INTEREST
    replies = True
    parent = None

    if json_obj is not None:
        # check if these parameters exist

        # check if limit exists
        if "limit" in json_obj:
            limit = json_obj["limit"]
            # make sure that the maximum is 100
            if limit > 100:
                limit = 100

        if "q" in json_obj:
            q = json_obj["q"]

        if "username" in json_obj:
            username = json_obj["username"]

        if "following" in json_obj:
            following = json_obj["following"]

        if "rank" in json_obj and json_obj["rank"] == api.RANK_BY_TIME:
            rank = json_obj["rank"]

        # only if json_obj["replies"] is False
        if "replies" in json_obj and not json_obj["replies"]:
            replies = json_obj["replies"]

        if "parent" in json_obj:
            parent = json_obj["parent"]

    resp = api.search(current_user, timestamp, limit, q, username, following, rank, parent, replies)
    items = resp["items"]
    for item in items:
        item["liked"] = api.get_is_liked_by(item["id"], current_user)
    return jsonify(resp)

#####################################################################
# Likes or unlikes a tweet ID
#
# Params:
#   like:
#       Boolean
#       Default: true
#
# Returns
# 	status: "OK" or "error"
# 	error: error message (if error)
@application.route('/item/<item_id>/like', methods=['POST'])
def like_or_unlike_item(item_id):
    # check of the user is logged in
    current_user = is_logged_in()
    if current_user is None:
        return jsonify({
            "status": "error",
            "error": "Please log in to like or unlike a tweet"
        })
    to_like = True
    json_obj = request.json
    if json_obj is not None and "like" in json_obj and json_obj["like"] == False:
        to_like = json_obj["like"]

    resp = api.like_or_unlike_item(current_user, to_like, item_id)
    return jsonify(resp)


#####################################################################
# Adds a media file (photo or video)
# Type is multipart/form-data
#
# Params:
#   content: binary content of file being uploaded
#
# Returns
# 	status: "OK" or "error"
#   id: ID of uploaded media
# 	error: error message (if error)
@application.route('/addmedia', methods=['POST'])
def add_media():
    # todo: set content
    file = request.files['content']
    content_type = file.content_type
    content = file.read()
    resp = api.add_media(content, content_type)
    # print(resp, file=sys.stderr)
    return jsonify(resp)

######################################################
# Gets media file by ID
# returns:
#   media file (image or video)
#
@application.route('/media/<media_id>', methods=['GET'])
def get_media(media_id):
    resp = api.get_media(media_id)
    if resp["status"] == "OK":
        content = resp["content"]
        content_type = resp["content_type"]

        return send_file(io.BytesIO(content), mimetype=content_type)
    else:
        return jsonify(resp)


######################################################
# Gets conversation from this speak forward
# returns:
#   media file (image or video)
#
@application.route('/getconv/<item_id>', methods=['GET'])
def getconv(item_id):
    items = []
    while item_id is not None and item_id != "None":
        item = api.get_item(item_id)["item"]

        items.append(item)
        item_id = item["parent"]
        #print(item_id, file=sys.stderr)
    return jsonify({"items": items})



#####################################################################
if __name__ == "__main__":
    application.run(host='0.0.0.0', debug=False, threaded=True, port=port_number)
