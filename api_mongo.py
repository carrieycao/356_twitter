
from datetime import datetime
from bson import ObjectId
import pymongo
import time

MAIN_PAGE = "/main"
INDEX_PAGE = "/"

BACKDOOR_CODE = "abracadabra"

client = pymongo.MongoClient("localhost", 27017)
db = client['twitter']
user_table = db['user_table']
tweet_table = db['tweet_table']
follow_table = db['follow_table']


#M3
like_table = db['like_table']

media = pymongo.MongoClient("localhost", 37021)
media_db = media['media']
media_table = media_db['media_table']
'''
create the media table using
mongo
use media
db.createCollection("media_table", {autoIndexId:true})
'''




'''
To connect to remote db server from one of the vm
mongo 192.168.1.34:27017/twitter -u spikker -p sp_pass


Creating the mongodb
mongo
use twitter
db.createCollection("user_table", {autoIndexId:true})
db.user_table.createIndex({"username" : 1}, {unique:true})
db.user_table.createIndex({"email" : 1}, {unique:true})
db.createCollection("tweet_table", {autoIndexId:true})
db.createCollection("follow_table", {autoIndexId:true})
db.follow_table.createIndex( { follower: 1, followed: 1 }, { unique: true })

# M3
db.createCollection("like_table", {autoIndexId:true})
db.like_table.createIndex({tweet_id: 1, username: 1}, {unique:true})


A follows B; A -> B
A is the follower
B is the followed

'''


#####################################################################
# returns "Unfollow" if current_user is following username
# "Follow" otherwise
def get_follow_btn_text(current_user, username):
    json = {
        'follower': current_user,
        'followed': username
    }
    res = follow_table.find_one(json)
    if res:
        return 'Unfollow'
    return "Follow"


#####################################################################
# returns the numnber of speaks made by username
# default -1
def get_number_speaks(username):
    json = {
        'username': username
    }
    res = tweet_table.find(json).count()
    if res is not None:
        return res
    return -1


#####################################################################
# Register new user account
# Username and email must be unique
# 1 Query
# Returns a dictionary object:
#   status: "OK" or "error"
#   error: error message (if error)
#   msg:  message (if ok)
#   key: activation key (if ok)
def add_user(username, email, password):
    json = {
        'username': username,
        'email': email,
        'password': password,
        'is_verified': False,
    }
    try:
        res = user_table.insert_one(json)
        resp = {
            "status": "OK",
            "key": str(res.inserted_id),
            "msg": "You account has been successfully created. " +
                   "Please check you email for activation link."
        }
        return resp
    except pymongo.errors.DuplicateKeyError:
        resp = {
            "status": "error",
            "error": "Username or email already in use"
        }
        return resp


#####################################################################
# Login to account
# 1 Query
# Returns a dictionary object:
#   status: "OK" or "error"
#   error: error message (if error)
#   session_id: session id (if ok)
#   redirect: link to the main page, MAIN_PAGE (if ok)
def login(username, password):
    json = {
        "username": username,
        "password": password
    }

    # find one and update returns document before modification
    res = user_table.find_one(json)

    resp = {}
    if res is None:
        resp = {
            'status': 'error',
            'error': 'invalid password or username'
        }

    elif res['is_verified']:
        resp = {
            "user_id": str(res['_id']),
            "status": "OK",
            "redirect": MAIN_PAGE_URL
        }

    elif not res['is_verified']:
        resp = {
            'status': 'error',
            'error': 'account not verified'
        }

    return resp


    #####################################################################


# Verifies account
# Account cannot be used until account is verified
# 1 Query
# Params:
# key: verification key, "abracadabra" should be a backdoor that works for any user
#
# Returns a dictionary object:
#   status: "OK" or "error"
#   error: error message (if error)
#   msg: message (if success)
def verify(email, key):
    if key == BACKDOOR_CODE:
        json_params = {
            "email": email
        }
    else:
        json_params = {
            "email": email,
            "_id": ObjectId(key)
        }

    json_opt = {
        "$set": {
            "is_verified": True
        }
    }

    # find one and upate returns document before modification
    res = user_table.find_one_and_update(json_params, json_opt)

    if res is None:
        resp = {
            'status': 'error',
            'error': 'Invalid email or key'
        }
        return resp

    elif not res['is_verified']:
        resp = {
            "user_id": str(res['_id']),
            "status": "OK",
            "msg": "You account has been verified."
        }
        return resp
    elif res['is_verified']:
        resp = {
            'status': 'error',
            'error': 'You account has already been verified'
        }
        return resp


#####################################################################
# Post a new tweet
# Only allowed if logged in. Session handled by server
#
# parent and media might be None
#
# Returns a dictionary object:
#   status: "OK" or "error"
#   id: unique tweet ID (if OK)
#   error: error message (if error)
def add_item(content, parent, media, username, insert_date):
    # DONE: M3 : parent and media
    json = {
        "content": content,
        "username": username,
        "num_likes": 0,
        "num_retweets": 0,
        "interests":0,
        "timestamp": insert_date,
        "parent_id": parent,
        "media_ids" : media
    }
    res = tweet_table.insert_one(json)
    resp = {
        'status': 'OK',
        'id': str(res.inserted_id)
    }
    return resp
    


#####################################################################
# Get contents of a single tweet given an ID
# 1 Query
# Params:
#   id: tweet id
#
# Returns
#   status: "OK" or "error"
#   item: {
#       id: tweet id
#       username: username who sent tweet
#       content: message body of tweet
#       timestamp: timestamp, represented as Unix time in seconds
#   }
#   error: error message (if error)
def get_item(id):
    # convert string id to ObjectId
    oid = ObjectId(id)
    json = {
        "_id": oid
    }
    res = tweet_table.find_one(json)

    if res is None:
        resp = {
            "status": "error",
            "error": "no tweets found with given id"
        }
        return resp
    else:
        # DONE : M3 : RETURN NUMBER OF LIKES, NUMBER OF RESPEAK AND MEDIA
        item = {
            "id": str(res["_id"]),
            "content": res["content"],
            "timestamp": str(int(time.mktime(res["timestamp"].timetuple()))),
            "num_likes": str(res["num_likes"]),
            "num_retweets": str(res["num_retweets"]),
            "interests": str(res["interests"]),
            "parent_id": str(res["parent_id"]),
            "username": res["username"],
            "media_ids": res["media_ids"]
        }
        resp = {
            "status": "OK",
            "item": item
        }
        return resp


#####################################################################
# Gets a list of all tweets
# Only allowed if logged in
#
#   Without options: 1 Query
#   With options: 2 Query (1 for getting list of following users)
# Params:
#   timestamp: search tweets from this time and earlier
#       Represented as Unix time in seconds
#
#   limit: number of tweets to return
#   username: if not None, query for only a single username 
#   q: if not None, query for string like this 
#   following: it not None, query for usernames in the following
#   rank: Order returned tweets by "time" or by "interest" (weighting time vs number of likes and retweets)
#   parent (boolean): Return tweets made in reply to requested tweet
#
# Returns (dict):
#   status: "OK" or "error"
#   items: Array of tweet objects (see /item/:id)
#   error: error message (if error)
def search(current_user, timestamp, limit, q, username, following_bool, rank, parent_id, replies):
    # DONE : M3
    resp = {
        "status": "OK",
        "items": []
    }
    json = {
        "timestamp": {"$lt": timestamp}
    }

    # new search with tokenize string and username and following_bool queries
    # If username is only param
    if username is not None and not following_bool:
        json["username"] = username

    # if query is not None
    if q is not None:
        ######## smarter search - uncomment q.replace ##########
        # replace space with '.*'
        # q = q.replace(" ", ".*")

        # case insensitive
        json["content"] = {'$regex': q, '$options': 'i'}

    # If following_bool is True
    if following_bool:
        following_list = get_who_user_is_following(current_user)

        # if username is not None
        if username is not None:
            # check if username is in the list of username
            in_following = False
            for user in following_list["users"]:
                if username == user:
                    in_following = True
                    break

            # if user is in the list of following
            if in_following:
                json["username"] = username

            # if user is not in the list of followings
            else:
                resp = {
                    "status": "OK",
                    "items": []
                }
                return resp

                # if username is None
        else:
            or_list = []
            for user in following_list['users']:
                or_list.append({"username": user})

            if len(or_list) > 0:
                json["$or"] = or_list
            if len(or_list) == 0:
                # no following; return
                return resp

    #DONE : M3:  replies and parent
    # if replies is true
    if replies:
        # if parent_id is not None, return only ones with parent_id
        if parent_id is not None:
            json['parent_id'] = parent_id

        # if parent_id is None, return everything, no need for constraint or modifications

    # if replies is false. Return only the ones with parent_id = null (non-replies)
    else:
        json['parent_id'] = 'null'
        
    if rank == "time":
        res = tweet_table.find(json, limit=limit).sort([("timestamp", -1)])
    else:
        res = tweet_table.find(json, limit=limit).sort([("interests", -1)])


    if not res:
        resp = {
            "status": "error",
            "error": "search failed for some unknown reason"
        }
        return resp

    # DONE: M3 : RETURN NUMBER OF LIKES AND NUMBER OF RESPEAK
    for re in res:
        resp["items"].append({
            "id": str(re["_id"]),
            "content": re["content"],
            "timestamp": str(int(time.mktime(re["timestamp"].timetuple()))),
            "num_likes": str(re["num_likes"]),
            "num_retweets": str(re["num_retweets"]),
            "interests": str(re["interests"]),
            "parent_id": str(re["parent_id"]),
            "username": re["username"],
            "media_ids": re["media_ids"]
        })

    return resp


#####################################################################
# Delete a single tweet given an ID
# 1 Query
# Params:
#   id: tweet id
#
# Returns
#   status: "OK" or "error"
#   error: error message (if error)
def delete_item(id, username):
    # DONE: M3 : DELETE MEDIA FILES
    oid = ObjectId(id)
    json = {
        "_id": oid,
        "username": username
    }
    #res = tweet_table.find_one_and_delete(json)
    res = tweet_table.find_one_and_delete(json)
    if res is None:
        resp = {
            "status": "error",
            "error": "no tweet with id found or the user is not the poster"
        }
        return resp
    else:
        # deleting the media files using the media ids
        media_ids = res['media_ids']
        o_ids = [];
        for m_id in media_ids:
            o_ids.append(ObjectId(m_id))

        json = {
            '_id': {'$in':o_ids} 
        }
        res = media_table.delete_many(json)
        resp = {
            "status": "OK",
        }
        return resp



#####################################################################
# Make a user follow another user. The current user
# is the following, the other is followed
# 2 Queries for follow, 1 Query for unfollow
# Params:
#   following: following user name
#   followed: followed user name
#   boole: True to follow, false to unfollow
#
# Returns
#   status: "OK" or "error"
#   error: error message (if error)
def follow(current_user, username, following_bool):
    # cannot be the same user
    if current_user == username:
        resp = {
            'status': 'error',
            'error': 'no matter how lonely you are, you can\'t follow or unfollow yourself :('
        }
        return resp

    if not following_bool:  # unfollowing username

        json = {
            'follower': current_user,
            'followed': username,
        }
        res = follow_table.find_one_and_delete(json)
        if res is None:
            resp = {
                "status": "error",
                "msg": "You are not following that person or that person doesn\'t exist"
            }
            return resp
        resp = {
            "status": "OK"
        }
        return resp

    # current_user want to follow username

    # check if username exists
    json = {
        "username": username
    }
    res = user_table.find_one(json)
    if res is None:
        res = {
            'status': 'error',
            'error': 'your friend doesn\'t exist'
        }
        return (res)

    # insert into db
    json = {
        'follower': current_user,
        'followed': username,
    }
    try:
        follow_table.insert_one(json)
        resp = {
            "status": "OK",
        }
        return resp
    except pymongo.errors.DuplicateKeyError:
        resp = {
            "status": "error",
            "error": "You are already following them"
        }
        return resp


#####################################################################
# Return a list of users the current user is following
# 1 Query
# Params:
#   username: current user
#   limit: list limit
#
# Returns
#   status: "OK" or "error"
#   error: error message (if error)
def get_who_user_is_following(username, limit=None):
    json = {
        "follower": username
    }
    if limit is not None:
        res = follow_table.find(json, limit=limit)
    else:
        res = follow_table.find(json)
    if res is None:
        resp = {
            "status": "error",
            "error": "get_who_user_is_following: internal db error"
        }
        return resp

    resp = {
        "status": "OK",
        "users": []
    }
    for re in res:
        resp["users"].append(re["followed"])
    return resp


#####################################################################
# Return a list of users that is following the current user
# 1 Query
# Params:
#   username: current user
#   limit: list limit
#
# Returns
#   status: "OK" or "error"
#   error: error message (if error)
def get_user_followers(username, limit):
    json = {
        "followed": username
    }

    res = follow_table.find(json, limit=limit)
    if res is None:
        resp = {
            "status": "error",
            "error": "get_user_followers: internal db error"
        }
        return resp

    resp = {
        "status": "OK",
        "users": []
    }
    for re in res:
        resp["users"].append(re["follower"])
    return resp


#####################################################################
# Return user's followers, number of users followed by user, and email
# 3 Queries (1 for email, one for following, one for followed)
# Params:
#   username: current user
#   limit: list limit
#
# Returns
#   status: "OK" or "error"
#   user: {
#       email
#       followers: follower count
#       following: following count
#   }
#   error: error message (if error)
def get_user(username):
    json = {
        "username": username,
    }

    res = user_table.find_one(json)
    if res is None:
        resp = {
            "status": "error",
            "error": "No such username in db"
        }

    # Return username -> x
    json = {
        "follower": username
    }
    following_count = follow_table.find(json).count()

    # Return x -> username
    json = {
        "followed": username
    }
    followers_count = follow_table.find(json).count()

    resp = {
        "status": "OK",
        "user": {
            "email": res["email"],
            "followers": followers_count,
            "following": following_count
        }
    }
    return resp


######################################################
# update like_table and num_likes in tweet_table
#
# current_user is the username of the user performing this action
# to_like is a boolean value
# item_id is the id of the item current_user liked/unliked
def like_or_unlike_item(current_user, to_like, item_id):
    # DONE: M3 : implement
    # The reason i am not checking the id at the beginning instead check when there is an error
    # is that the chance of second happening is a lot less, why impede a extra query when most 
    # of the times the item_id will not be wrong
    item_json = { 
        '_id': ObjectId(item_id)
    }
    json = {
        'tweet_id': ObjectId(item_id),
        'username': current_user
    }
    # if to_like is true, this adds item to the table.
    if to_like:
        # try to see if there is already a duplicate value
        try:
            like_table.insert_one(json)

            # update the num likes
            item_json_opt = {
                '$inc':{'num_likes':1, 'interests':1}
            }
            res = tweet_table.find_one_and_update(item_json, item_json_opt)

            if res is None:
                # undo the changes
                like_table.find_one_and_delete(json)
                resp = {
                    "status": "error",
                    "error": "item_id doesnt exist"
                }
                return resp 

            resp = {
                "status": "OK",
            }
            return resp

        except pymongo.errors.DuplicateKeyError:
            resp = {
                "status": "error",
                "error": "You are already liked the tweet"
            }
            return resp
    
    # if to_like is false, delete the item from the table
    res = like_table.find_one_and_delete(json)
    # if the result returned is not none
    if res is not None:
        #decrement num_likes and interests
        item_json_opt = {
            '$inc':{'num_likes':-1, 'interests':-1}
        }
        res = tweet_table.find_one_and_update(item_json,item_json_opt)

        if res is None:
                # undo the changes
                like_table.insert_one(json)
                resp = {
                    "status": "error",
                    "error": "item_id doesnt exist"
                }
                return resp 

        resp = {
            "status": "OK",
        }
        return resp
    # if res is None
    resp = {
        "status": "error",
        "error": "You can't unlike a liked post"
    }
    return resp 
        
    

######################################################
# stores content in media_table and
# returns:
#   status: "OK" or "error"
#   id: ID of uploaded media
#   error: error message (if error)
#
# content: binary content of a file
def add_media(content):
    # DONE: IMPLEMENT
    # make json and connects to another db and insert it into db
    json = {
        "content": content
    }
    # get the id returned

    res = media_table.insert_one(json)
    if res is not None:
        resp = {
            'status' : 'OK',
            'id' : res.inserted_id
        }
        return resp 
    resp = {
        'status' : 'error',
        'error' : 'failed to insert in to media table'
    }
    return resp 



######################################################
# Gets media file by ID
# returns:
#   status: "OK" or "error"
#   content: binary content of a file (if okay)
#   error: error message (if error)
#
def get_media(media_id):
    # DONE: IMPLEMENT
    
    o_id = ObjectId(media_id)
    json = {
        "_id": o_id
    }
    res = media_table.find_one(json)
    if res is not None:
        resp = {
            'status': 'OK',
            'content': res['content']
        }
        return resp 

    resp = {
        'status' : 'error',
        'error' : 'failed to find ' + str(media_id) + ' in the media table'
    }
    return resp 


######################################################
# returns true if username has liked item_id,
# false other wise
def get_is_liked(item_id, username):
    # DONE: M3: IMPLEMENT
    json = {
        'username' : username,
        'tweet_id' : ObjectId(item_id)
    }
    res = like_table.find_one(json)
    if res is None:
        return False
    return True

if (__name__ =='__main__'):
    
    #pprint.pprint(user_table.find_one())
    
    a = datetime.now()

    #add_item(content, parent, media, username, insert_date):
    
    
    
    '''
    add_user #############################################
    #18.926295
    for i in range(0, 50000):
        add_user(str(i), str("email: " + str(i)), "1234")
        if(i % 1000 == 0):
            print("Created: " + str(i) + " Users")
    #add_user('carrie', 'carrie@gmail', '1234')
    '''
    '''
    verify ################################################
    22.653598
    for i in range(0, 50000):
        verify("email: " + str(i), "abracadabra")
        if(i % 1000 == 0):
            print("Verified: " + str(i) + " Users")
    '''

    '''
    #login ##################################################
    #21.558492
    #20.284688 if query for username only then for password
    for i in range(0, 50000):
        login(str(i), "1234")
        if(i % 1000 == 0):
            print("Logged in: " + str(i) + " Users")
    '''
    

    '''
    #add_item ###############################################
    #17.534089
    #18.048295 for parent id added 
    for i in range(0,50000):
        add_item (str(i), "yuzhe", datetime.utcnow())
        if(i % 1000 == 0):
            print("Added: " + str(i) + "Items")
    '''
    #add_item("hello", "1", datetime.utcnow())
    #add_item("hello2", "2", datetime.utcnow())




    #single search took 00.127390 seconds
    #res = search('yuzhe', datetime.utcnow(), 25 , following_bool= True, username = '3')
    #items = res["items"]
    # 1 hello, 2 hello2
    #for item in items:
        #print(item)



    #print(str(follow("yuzhe", "101", True)))
    '''
    for i in range (0,100):
        follow( str(i), "yuzhe")
    '''



    #query time 0.004802 
    #print(following('yuzhe'))
    #print(followed("yuzhe", 100))
    #print(user('yuzhe'))


    #58dc63d55800d71871b1ac98
    #print(add_item("hello", "yuzhe", datetime.utcnow()))
    #print(delete_item("58dc63d55800d71871b1ac98"))




    #M3
    #id1 = '58ee87145800d710c3dc3ea0'
    #id2 = '58ee87145800d710c3dc3ea1'
    #add_item('like test', 'null', [], 'yuzhe', datetime.now())
    #print(get_item("58eea0055800d71893b3b5ea"))
    #delete_item ("58eea09f5800d718f9ac5e9e", "yuzhe")

    #id1 = add_media("1")
    
    #print(get_media(id1['id']))


    #tweet id = 58eea16d5800d7195703082a
    #print(like_or_unlike_item('yuzhe h', True, '58eea16d5800d7195703082a'))
    print(get_is_liked('58eea16d5800d7195703082a','yuzhe hu' ))


    b = datetime.now()
    print(b-a)


    #pprint.pprint(verify("yuzhe@gmail", "58dc24a9000c90c642e97c6b"))
    #print(login("Yuzhe", "1234"))

    #this will continuously run the app
    #app.run(host='0.0.0.0', port=80)