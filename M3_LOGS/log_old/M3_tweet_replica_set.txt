sudo vi /etc/mongod.conf
## comment out authorization
sudo service mongod stop

======== part 1 - configsvr ============
# run the following on all members

	# first make sure that ~/data, ~/data/configsvr,  and ~/data/logs exist
	mkdir data
	cd data
	mkdir configsvr
	mkdir logs
	sudo mongod --configsvr --replSet cf_rs --dbpath ~/data/configsvr --fork --logpath ~/data/logs/log_1 --port 27019

# DB1 only
mongo localhost:27019

rs.initiate(
  {
    _id: "cf_rs",
    configsvr: true,
    members: [
      { _id : 0, host : "192.168.1.66:27019" },
      { _id : 1, host : "192.168.1.64:27019" },
      { _id : 2, host : "192.168.1.65:27019" }
    ]
  }
)


======= part 2 - a 3-member replica-set shard ===============
# run the following on all members

	# first make sure that ~/data, ~/data/shardsvr,  and ~/data/logs exist
	cd ~/data
	mkdir shardsvr
	sudo mongod --shardsvr --replSet rs1 --dbpath ~/data/shardsvr --fork --logpath ~/data/logs/log_2 --port 37017

# DB1 only
mongo localhost:37017

rs.initiate(
  {
    _id: "rs1",
    members: [
      { _id : 0, host : "192.168.1.66:37017" },
      { _id : 1, host : "192.168.1.64:37017" },
      { _id : 2, host : "192.168.1.65:37017" }
    ]
  }
)


====== part 3 - connecting using mongos ===========

# DB1 - make sure nothing is running on 27017
	sudo service mongod stop
	sudo mongos --configdb cf_rs/192.168.1.66:27019,192.168.1.65:27019,192.168.1.64:27019 --fork --logpath ~/data/logs/log_3

	mongo localhost:27017

	sh.addShard('rs1/192.168.1.66:37017')
	
# use mongo localhost:27017 to start mongos






